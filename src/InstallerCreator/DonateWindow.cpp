﻿#include "DonateWindow.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QPixmap>
#include <QSizePolicy>
#include <QDesktopServices>
#include <QEvent>
#include <QMouseEvent>

DonateWindow::DonateWindow(QWidget *parent) : QWidget(parent)
{
    setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setWindowTitle(tr("Donate"));
    QLabel *m_pDonateQCodeImage = new QLabel(this);
    m_pDonateQCodeImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_pDonateQCodeImage->setScaledContents(true);
    m_pDonateQCodeImage->setPixmap(QPixmap(":/images/DonateQCode.png"));
    m_pDonateQCodeImage->installEventFilter(this);
    QVBoxLayout *m_pMainLayout = new QVBoxLayout(this);
    m_pMainLayout->setContentsMargins(0, 0, 0, 0);
    m_pMainLayout->setSpacing(0);
    setLayout(m_pMainLayout);
    m_pMainLayout->addWidget(m_pDonateQCodeImage);
}

DonateWindow::~DonateWindow()
{

}

void DonateWindow::openDonateURL()
{
    QDesktopServices::openUrl(QUrl(QStringLiteral(APP_DONATE_URL)));
    close();
}

bool DonateWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *mouse_event = static_cast<QMouseEvent *>(event);
        if (mouse_event->button() == Qt::LeftButton)
        {
            openDonateURL();
            return true;
        }
    }
    return QWidget::eventFilter(obj, event);
}
