APP_NAME=InstallerCreator
APP_DISPLAY_NAME=InstallerCreator
VERSION = 1.0.0.0
APP_PUBLISHER=wangwenx190
APP_COPYRIGHT=GPLv3

DEFINES += APP_ID=\\\"{F2A59762-4B8B-45DF-B655-4F7B2EA5AD27}\\\"
DEFINES += APP_NAME=\\\"$$APP_NAME\\\"
DEFINES += APP_DISPLAY_NAME=\\\"$$APP_DISPLAY_NAME\\\"
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DEFINES += APP_PUBLISHER=\\\"$$APP_PUBLISHER\\\"
DEFINES += APP_PUBLISHER_URL=\\\"https://gitee.com/wangwenx190\\\"
DEFINES += APP_COPYRIGHT=\\\"$$APP_COPYRIGHT\\\"
DEFINES += APP_LICENSE_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator/blob/master/LICENSE.md\\\"
DEFINES += APP_DONATE_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator\\\"
DEFINES += APP_README_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator/blob/master/README.md\\\"
DEFINES += APP_BUGREPORT_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator/issues\\\"
DEFINES += APP_PULLREQUEST_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator/pulls\\\"
DEFINES += APP_SOURCECODE_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator\\\"
DEFINES += APP_WIKI_URL=\\\"https://gitee.com/wangwenx190/Installer-Creator/wikis\\\"

QMAKE_TARGET_COMPANY = "$$APP_PUBLISHER"
QMAKE_TARGET_COPYRIGHT = "$$APP_COPYRIGHT"
QMAKE_TARGET_DESCRIPTION = "$$APP_DISPLAY_NAME"
QMAKE_TARGET_PRODUCT = "$$APP_NAME"
