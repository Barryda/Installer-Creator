﻿#pragma once

#include "core_global.h"

class CORESHARED_EXPORT Core
{

public:
    Core();
    int exec(int argc, char *argv[]);
};
