﻿#include "ConfigManager.h"

#include <QCoreApplication>
#include <QDesktopWidget>
#include <QLocale>
#include <QTextCodec>

ConfigManager *ConfigManager::Instance()
{
    static ConfigManager configManager;
    return &configManager;
}

void ConfigManager::reload()
{
    QDesktopWidget desktop;
    settings->beginGroup(QStringLiteral("General"));
    lastGeometry = settings->value(QStringLiteral("lastGeometry"), desktop.availableGeometry()).toRect();
    donated = settings->value(QStringLiteral("donated"), false).toBool();
    donator = settings->value(QStringLiteral("donator"), QString()).toString();
    lastFile = settings->value(QStringLiteral("lastFile"), QCoreApplication::applicationFilePath()).toString();
    recentFileList = settings->value(QStringLiteral("recentFileList"), QStringList()).toStringList();
    overrideLanguage = settings->value(QStringLiteral("overrideLanguage"), QLocale::system().name()).toString();
    alwaysCheckAssoc = settings->value(QStringLiteral("alwaysCheckAssoc"), true).toBool();
    shouldAssoc = settings->value(QStringLiteral("shouldAssoc"), true).toBool();
    uninstallerPath = QString();
    settings->endGroup();
}

void ConfigManager::save()
{
    settings->beginGroup(QStringLiteral("General"));
    settings->setValue(QStringLiteral("lastGeometry"), lastGeometry);
    settings->setValue(QStringLiteral("donated"), donated);
    settings->setValue(QStringLiteral("donator"), donator);
    settings->setValue(QStringLiteral("lastFile"), lastFile);
    settings->setValue(QStringLiteral("recentFileList"), recentFileList);
    settings->setValue(QStringLiteral("overrideLanguage"), overrideLanguage);
    settings->setValue(QStringLiteral("alwaysCheckAssoc"), alwaysCheckAssoc);
    settings->setValue(QStringLiteral("shouldAssoc"), shouldAssoc);
    settings->endGroup();
}

void ConfigManager::setLastGeometry(const QRect &m_geometry)
{
    lastGeometry = m_geometry;
}

void ConfigManager::setDonated(bool m_donated)
{
    donated = m_donated;
}

void ConfigManager::setDonator(const QString &m_donator)
{
    donator = m_donator;
}

void ConfigManager::setLastFile(const QString &m_path)
{
    lastFile = m_path;
}

void ConfigManager::setRecentFileList(const QStringList &m_list)
{
    recentFileList = m_list;
}

void ConfigManager::setOverrideLanguage(const QString &m_lang)
{
    overrideLanguage = m_lang;
}

void ConfigManager::setUninstallerPath(const QString &m_uninst)
{
    uninstallerPath = m_uninst;
}

void ConfigManager::setAlwaysCheckAssoc(bool m_always)
{
    alwaysCheckAssoc = m_always;
}

void ConfigManager::setShouldAssoc(bool m_should)
{
    shouldAssoc = m_should;
}

QRect ConfigManager::getLastGeometry() const
{
    return lastGeometry;
}

bool ConfigManager::isDonated() const
{
    return donated;
}

QString ConfigManager::getDonator() const
{
    return donator;
}

QString ConfigManager::getLastFile() const
{
    return lastFile;
}

QStringList ConfigManager::getRecentFileList() const
{
    return recentFileList;
}

QString ConfigManager::getOverrideLanguage() const
{
    return overrideLanguage;
}

QString ConfigManager::getUninstallerPath() const
{
    return uninstallerPath;
}

bool ConfigManager::shouldAlwaysCheckAssoc() const
{
    return alwaysCheckAssoc;
}

bool ConfigManager::shouldAssocNow() const
{
    return shouldAssoc;
}

ConfigManager::ConfigManager()
{
    settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings->setIniCodec(QTextCodec::codecForName("UTF-8"));
    reload();
}

ConfigManager::~ConfigManager()
{
    save();
    delete settings;
    settings = nullptr;
}
