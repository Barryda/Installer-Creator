QT += core gui widgets

TEMPLATE = lib

DEFINES += CORE_LIBRARY QT_DEPRECATED_WARNINGS

include(version.pri)

win32: {
contains(QT_ARCH, x86_64) {
CONFIG(debug, debug|release) {
    TARGET = InstallerCreatorCored64
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win64_Debug
} else {
    TARGET = InstallerCreatorCore64
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win64_Release
}
} else {
CONFIG(debug, debug|release) {
    TARGET = InstallerCreatorCored
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win32_Debug
} else {
    TARGET = InstallerCreatorCore
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win32_Release
}
}
}

include(deploy.pri)

INCLUDEPATH += \
    $$PWD \
    $$PWD/qui

DEPENDPATH += \
    $$PWD \
    $$PWD/qui

PRECOMPILED_HEADER = PCH.h

HEADERS += \
    Core.h \
    core_global.h \
    MainWindow.h \
    PCH.h \
    DonateWindow.h \
    WelcomeScreen.h \
    DraggableWidget.h \
    ConfigManager.h \
    WindowManager.h \
    Utils.h \
    navlistview.h \
    sliderbar.h

SOURCES += \
    Core.cpp \
    MainWindow.cpp \
    DonateWindow.cpp \
    WelcomeScreen.cpp \
    DraggableWidget.cpp \
    ConfigManager.cpp \
    WindowManager.cpp \
    Utils.cpp \
    navlistview.cpp \
    sliderbar.cpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += resources/coreResources.qrc

TRANSLATIONS += \
    resources/i18n/ic_zh_CN.ts \
    resources/i18n/ic_zh_TW.ts
