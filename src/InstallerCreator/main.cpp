﻿#pragma execution_character_set("UTF-8")

#include "Core.h"

int main(int argc, char *argv[])
{
    Core core;
    return core.exec(argc, argv);
}
