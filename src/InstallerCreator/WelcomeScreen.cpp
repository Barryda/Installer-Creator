﻿#include "WelcomeScreen.h"
#include "ConfigManager.h"
#include "Utils.h"

#include <QColor>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFont>
#include <QPalette>
#include <QToolButton>
#include <QAction>
#include <QSpacerItem>
#include <QTimer>
#include <QEvent>
#include <QMouseEvent>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QCloseEvent>

WelcomeScreen::WelcomeScreen(QWidget *parent) : DraggableWidget(parent)
  , m_pDonateWindow(nullptr)
  , m_pMainLayout(nullptr)
  , m_pRecentFileListPanel(nullptr)
  , m_pTemplateChooserPanelArea(nullptr)
  , m_pRecentFileListArea(nullptr)
  , m_pWindowTitleBarPanel(nullptr)
  , m_pMinimizeButton(nullptr)
  , m_pMaximizeButton(nullptr)
  , m_pCloseButton(nullptr)
{
    initForm();

    m_pDonateWindow = new DonateWindow();
    if (!ConfigManager::Instance()->isDonated())
    {
        QTimer::singleShot(3000, this, SLOT(showDonateWindow()));
    }
}

WelcomeScreen::~WelcomeScreen()
{
    if (m_pDonateWindow)
    {
        m_pDonateWindow->close();
        delete m_pDonateWindow;
        m_pDonateWindow = nullptr;
    }
}

bool WelcomeScreen::eventFilter(QObject *obj, QEvent *event)
{
    if (obj->objectName() == QStringLiteral("titleBarWidget"))
    {
        if (event->type() == QEvent::MouseButtonDblClick)
        {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            if (mouseEvent->button() == Qt::LeftButton)
            {
                onMaximizeButtonClicked();
                return true;
            }
        }
    }
    return DraggableWidget::eventFilter(obj, event);
}

void WelcomeScreen::closeEvent(QCloseEvent *event)
{
    if (confirmExit())
    {
        saveConfig();
        DraggableWidget::closeEvent(event);
        event->accept();
    }
    else
    {
        event->ignore();
    }
}

void WelcomeScreen::onMinimizeButtonClicked()
{
    showMinimized();
}

void WelcomeScreen::onMaximizeButtonClicked()
{
    if (isMaximized())
    {
        showNormal();
        IconHelper::Instance()->setIcon(m_pMaximizeButton, QUIConfig::IconNormal, 10);
    }
    else
    {
        showMaximized();
        IconHelper::Instance()->setIcon(m_pMaximizeButton, QUIConfig::IconMax, 10);
    }
}

void WelcomeScreen::onCloseButtonClicked()
{
    if (m_pDonateWindow->isVisible())
    {
        m_pDonateWindow->close();
    }
    close();
}

void WelcomeScreen::showDonateWindow()
{
    if (m_pDonateWindow == nullptr)
    {
        m_pDonateWindow = new DonateWindow();
    }
    m_pDonateWindow->show();
    Utils::setFormInCenter(m_pDonateWindow);
}

void WelcomeScreen::createRecentFileListPanel()
{
    m_pRecentFileListPanel = new QWidget(this);
    m_pRecentFileListPanel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pRecentFileListPanel->setFixedWidth(576);
    //QPalette pal;
    //pal.setColor(QPalette::Background, QColor(43, 87, 154));
    m_pRecentFileListPanel->setAutoFillBackground(true);
    //m_pRecentFileListPanel->setPalette(pal);
    m_pRecentFileListPanel->setMouseTracking(true);
    m_pRecentFileListPanel->setStyleSheet(QStringLiteral("QWidget{background-color: rgb(43, 87, 154);}"));

    QLabel *m_pTitleLabel = new QLabel(m_pRecentFileListPanel);
    QFont fnt;
    //fnt.setFamily(QStringLiteral("Arial"));
    fnt.setPointSize(25);
    m_pTitleLabel->setFont(fnt);
    //pal.setColor(QPalette::WindowText, Qt::white);
    //m_pTitleLabel->setPalette(pal);
    m_pTitleLabel->setText(QStringLiteral(APP_NAME));
    m_pTitleLabel->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));

    QLabel *m_pSubTitleLabel = new QLabel(m_pRecentFileListPanel);
    fnt.setPointSize(15);
    m_pSubTitleLabel->setFont(fnt);
    //m_pSubTitleLabel->setPalette(pal);
    m_pSubTitleLabel->setText(tr("Recent projects"));
    m_pSubTitleLabel->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 255, 255);}"));

    QWidget *m_pSeparatorWidget = new QWidget(m_pRecentFileListPanel);
    m_pSeparatorWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_pSeparatorWidget->setFixedHeight(1);
    m_pSeparatorWidget->setAutoFillBackground(true);
    m_pSeparatorWidget->setStyleSheet(QStringLiteral("QWidget{background-color: rgba(255, 255, 255, 0.3);}"));

    QToolButton *m_pOpenProjectToolButton = new QToolButton(m_pRecentFileListPanel);
    m_pOpenProjectToolButton->setArrowType(Qt::NoArrow);
    m_pOpenProjectToolButton->setStyleSheet(QStringLiteral("QToolButton{border: none; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0);}"));
    QAction *m_pOpenProjectToolButtonAction = new QAction(m_pRecentFileListPanel);
    m_pOpenProjectToolButtonAction->setText(tr("Open project"));
    m_pOpenProjectToolButtonAction->setIcon(QIcon(":/images/fileopen_32px.png"));
    m_pOpenProjectToolButton->setIconSize(QSize(20, 20));
    fnt.setPointSize(10);
    m_pOpenProjectToolButton->setFont(fnt);
    m_pOpenProjectToolButton->setDefaultAction(m_pOpenProjectToolButtonAction);
    m_pOpenProjectToolButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    QWidget *m_pRecentFileList = new QWidget(m_pRecentFileListPanel);
    m_pRecentFileList->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pRecentFileList->setMinimumWidth(576);
    m_pRecentFileList->setMouseTracking(true);
    QVBoxLayout *m_pRecentFileListLayout = new QVBoxLayout(m_pRecentFileList);
    m_pRecentFileListLayout->setContentsMargins(0, 0, 0, 0);
    m_pRecentFileListLayout->setSpacing(0);
    m_pRecentFileList->setLayout(m_pRecentFileListLayout);
    m_pRecentFileListLayout->addWidget(m_pOpenProjectToolButton);
    m_pRecentFileListLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
    m_pRecentFileListArea = new QScrollArea(m_pRecentFileListPanel);
    m_pRecentFileListArea->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pRecentFileListArea->setMinimumWidth(576);
    m_pRecentFileListArea->setWidget(m_pRecentFileList);
    m_pRecentFileListArea->setWidgetResizable(true);

    QVBoxLayout *m_pRecentFileListPanelLayout = new QVBoxLayout(m_pRecentFileListPanel);
    m_pRecentFileListPanelLayout->setContentsMargins(30, 30, 30, 30);
    m_pRecentFileListPanelLayout->setSpacing(10);
    m_pRecentFileListPanel->setLayout(m_pRecentFileListPanelLayout);
    m_pRecentFileListPanelLayout->addWidget(m_pTitleLabel);
    m_pRecentFileListPanelLayout->addWidget(m_pSubTitleLabel);
    m_pRecentFileListPanelLayout->addWidget(m_pSeparatorWidget);
    m_pRecentFileListPanelLayout->addWidget(m_pRecentFileListArea);
    //m_pRecentFileListPanelLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
}

void WelcomeScreen::createTemplateChooserPanel()
{
    QWidget *m_pTemplateChooserPanel = new QWidget(this);
    m_pTemplateChooserPanel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_pTemplateChooserPanel->setMouseTracking(true);
    m_pTemplateChooserPanelArea = new QScrollArea(this);
    m_pTemplateChooserPanelArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_pTemplateChooserPanelArea->setWidget(m_pTemplateChooserPanel);
    m_pTemplateChooserPanelArea->setWidgetResizable(true);
}

void WelcomeScreen::createWindowTitleBar()
{
    m_pWindowTitleBarPanel = new QWidget(this);
    m_pWindowTitleBarPanel->setObjectName(QStringLiteral("titleBarWidget"));
    m_pWindowTitleBarPanel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_pWindowTitleBarPanel->setFixedHeight(32);
    m_pWindowTitleBarPanel->setMouseTracking(true);

    m_pMinimizeButton = new QPushButton(m_pWindowTitleBarPanel);
    m_pMinimizeButton->setObjectName(QStringLiteral("minimizeButton"));
    m_pMinimizeButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pMinimizeButton->setMinimumSize(46, 32);
    IconHelper::Instance()->setIcon(m_pMinimizeButton, QUIConfig::IconMin, 13);
    m_pMaximizeButton = new QPushButton(m_pWindowTitleBarPanel);
    m_pMaximizeButton->setObjectName(QStringLiteral("maximizeButton"));
    m_pMaximizeButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pMaximizeButton->setMinimumSize(46, 32);
    IconHelper::Instance()->setIcon(m_pMaximizeButton, QUIConfig::IconNormal, 10);
    m_pCloseButton = new QPushButton(m_pWindowTitleBarPanel);
    m_pCloseButton->setObjectName(QStringLiteral("closeButton"));
    m_pCloseButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_pCloseButton->setMinimumSize(46, 32);
    IconHelper::Instance()->setIcon(m_pCloseButton, QUIConfig::IconClose, 13);

    QHBoxLayout *m_pTitleBarLayout = new QHBoxLayout(m_pWindowTitleBarPanel);
    m_pTitleBarLayout->setContentsMargins(0, 0, 0, 0);
    m_pTitleBarLayout->setSpacing(0);
    m_pWindowTitleBarPanel->setLayout(m_pTitleBarLayout);
    m_pTitleBarLayout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    m_pTitleBarLayout->addWidget(m_pMinimizeButton);
    m_pTitleBarLayout->addWidget(m_pMaximizeButton);
    m_pTitleBarLayout->addWidget(m_pCloseButton);

    connect(m_pMinimizeButton, SIGNAL(clicked()), this, SLOT(onMinimizeButtonClicked()));
    connect(m_pMaximizeButton, SIGNAL(clicked()), this, SLOT(onMaximizeButtonClicked()));
    connect(m_pCloseButton, SIGNAL(clicked()), this, SLOT(onCloseButtonClicked()));

    m_pWindowTitleBarPanel->installEventFilter(this);
}

void WelcomeScreen::initForm()
{
    setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    setWindowTitle(QStringLiteral(APP_NAME));

    createWindowTitleBar();
    createRecentFileListPanel();
    createTemplateChooserPanel();

    QVBoxLayout *m_pContentAreaLayout = new QVBoxLayout();
    m_pContentAreaLayout->setContentsMargins(0, 0, 0, 0);
    m_pContentAreaLayout->setSpacing(0);
    m_pContentAreaLayout->addWidget(m_pWindowTitleBarPanel);
    m_pContentAreaLayout->addWidget(m_pTemplateChooserPanelArea);

    m_pMainLayout = new QHBoxLayout(this);
    m_pMainLayout->setContentsMargins(0, 0, 0, 0);
    m_pMainLayout->setSpacing(0);
    setLayout(m_pMainLayout);    
    m_pMainLayout->addWidget(m_pRecentFileListPanel);
    m_pMainLayout->addLayout(m_pContentAreaLayout);

    setAutoFillBackground(true);
    setMouseTracking(true);

    QSize minSize(1280, 720);
    setMinimumSize(minSize);

    QDesktopWidget desktop;
    QRect maxGeometry = desktop.availableGeometry();
    QRect lastGeometry = ConfigManager::Instance()->getLastGeometry();
    if (lastGeometry == maxGeometry)
    {
        onMaximizeButtonClicked();
    }
    else
    {
        setGeometry(lastGeometry);
    }
}

bool WelcomeScreen::confirmExit()
{
    if (QMessageBox::question(this, tr("Question"), tr("Sure to exit ?")) == QMessageBox::Yes)
    {
        return true;
    }
    return false;
}

void WelcomeScreen::saveConfig()
{
    ConfigManager::Instance()->setLastGeometry(geometry());
    ConfigManager::Instance()->save();
}
