﻿#pragma once

#include <QSettings>
#include <QRect>
#include <QString>
#include <QStringList>

class ConfigManager
{
public:
    static ConfigManager *Instance();
    void reload();
    void save();
    void setLastGeometry(const QRect &m_geometry);
    void setDonated(bool m_donated);
    void setDonator(const QString &m_donator);
    void setLastFile(const QString &m_path);
    void setRecentFileList(const QStringList &m_list);
    void setOverrideLanguage(const QString &m_lang);
    void setUninstallerPath(const QString &m_uninst);
    void setAlwaysCheckAssoc(bool m_always);
    void setShouldAssoc(bool m_should);
    QRect getLastGeometry() const;
    bool isDonated() const;
    QString getDonator() const;
    QString getLastFile() const;
    QStringList getRecentFileList() const;
    QString getOverrideLanguage() const;
    QString getUninstallerPath() const;
    bool shouldAlwaysCheckAssoc() const;
    bool shouldAssocNow() const;

private:
    ConfigManager();
    ~ConfigManager();

private:
    QSettings *settings;
    QRect lastGeometry;
    bool donated;
    QString donator;
    QString lastFile;
    QStringList recentFileList;
    QString overrideLanguage;
    QString uninstallerPath;
    bool alwaysCheckAssoc;
    bool shouldAssoc;
};
