SETLOCAL
SET PARAM01=%1
SET PARAM02=%2
SET PARAM03=%3
SET PARAM04=%4
SET _QT_BIN_DIR="%PARAM01:~1,-1%\bin"
SET _QT_QM_DIR="%PARAM01:~1,-1%\translations"
SET _BIN_DIR="%PARAM02:~1,-1%"
SET _BIN_FILE_NAME_EXE="%PARAM03:~1,-1%.exe"
SET _BIN_FILE_NAME_DLL="%PARAM03:~1,-1%1.dll"
SET _WIN_DEPLOY_QT_TOOL_EXE="%_QT_BIN_DIR:~1,-1%\windeployqt.exe"
IF EXIST "%_BIN_DIR:~1,-1%" (
CD /D "%_BIN_DIR:~1,-1%"
IF EXIST "%_BIN_FILE_NAME_EXE:~1,-1%" "%_WIN_DEPLOY_QT_TOOL_EXE:~1,-1%" "%_BIN_FILE_NAME_EXE:~1,-1%"
IF EXIST "%_BIN_FILE_NAME_DLL:~1,-1%" "%_WIN_DEPLOY_QT_TOOL_EXE:~1,-1%" "%_BIN_FILE_NAME_DLL:~1,-1%"
REM IF EXIST "%_QT_BIN_DIR:~1,-1%\libeay32.dll" COPY /Y "%_QT_BIN_DIR:~1,-1%\libeay32.dll" "%_BIN_DIR:~1,-1%"
REM IF EXIST "%_QT_BIN_DIR:~1,-1%\ssleay32.dll" COPY /Y "%_QT_BIN_DIR:~1,-1%\ssleay32.dll" "%_BIN_DIR:~1,-1%"
IF NOT EXIST "%_BIN_DIR:~1,-1%\translations" MD "%_BIN_DIR:~1,-1%\translations"
COPY /Y "%_QT_QM_DIR:~1,-1%\qt_*.qm" "%_BIN_DIR:~1,-1%\translations"
DEL /F /Q "%_BIN_DIR:~1,-1%\translations\qt_help_*.qm"
COPY /Y "%_BIN_DIR:~1,-1%\..\..\src\InstallerCreator\resources\i18n\*.qm" "%_BIN_DIR:~1,-1%\translations"
IF "%PARAM04%" == "release" (
DEL /F /Q "*.pdb"
DEL /F /Q "*.ilk"
DEL /F /Q "*.exp"
DEL /F /Q "*.lib"
COPY /Y "%_BIN_DIR:~1,-1%\..\..\doc\*" "%_BIN_DIR:~1,-1%"
)
IF EXIST "%_BIN_DIR:~1,-1%\templates" RD /S /Q "%_BIN_DIR:~1,-1%\templates"
MD "%_BIN_DIR:~1,-1%\templates"
XCOPY /E /I /H /R /Y "%_BIN_DIR:~1,-1%\..\..\src\InstallerCreator\resources\templates" "%_BIN_DIR:~1,-1%\templates"
CD /D "%_BIN_DIR:~1,-1%\templates\default\lib"
REN "*.1" "*.dll"
)
ENDLOCAL