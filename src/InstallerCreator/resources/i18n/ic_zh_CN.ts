<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DonateWindow</name>
    <message>
        <location filename="../../DonateWindow.cpp" line="15"/>
        <source>Donate</source>
        <translation>捐助</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Setting up application...</source>
        <translation type="vanished">正在初始化程序……</translation>
    </message>
    <message>
        <location filename="../../Core.cpp" line="44"/>
        <source>Creating main window...</source>
        <translation>正在创建主窗口……</translation>
    </message>
</context>
<context>
    <name>WelcomeScreen</name>
    <message>
        <location filename="../../WelcomeScreen.cpp" line="124"/>
        <source>Recent projects</source>
        <translation>最近工程</translation>
    </message>
    <message>
        <location filename="../../WelcomeScreen.cpp" line="137"/>
        <source>Open project</source>
        <translation>打开工程</translation>
    </message>
</context>
</TS>
