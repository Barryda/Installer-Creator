TEMPLATE = app

include(version.pri)

win32: {
contains(QT_ARCH, x86_64) {
CONFIG(debug, debug|release) {
    TARGET = InstallerCreatord64
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win64_Debug
    LIBS += \
        -L$$DESTDIR \
        -lInstallerCreatorCored641
} else {
    TARGET = InstallerCreator64
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win64_Release
    LIBS += \
        -L$$DESTDIR \
        -lInstallerCreatorCore641
}
} else {
CONFIG(debug, debug|release) {
    TARGET = InstallerCreatord
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win32_Debug
    LIBS += \
        -L$$DESTDIR \
        -lInstallerCreatorCored1
} else {
    TARGET = InstallerCreator
    DESTDIR = $$PWD/../../bin/InstallerCreator_Win32_Release
    LIBS += \
        -L$$DESTDIR \
        -lInstallerCreatorCore1
}
}
}

win32: {
RC_ICONS = resources/icons/InstallerCreator.ico
Release:QMAKE_POST_LINK += $$quote(\"$$PWD/windeploy.bat\" \"$$(QTDIR)\" \"$$DESTDIR\" \"$$TARGET\" release$$escape_expand(\n\t))
}

SOURCES = main.cpp
