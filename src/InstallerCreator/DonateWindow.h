﻿#pragma once

#include <QWidget>

class DonateWindow : public QWidget
{
    Q_OBJECT
public:
    explicit DonateWindow(QWidget *parent = nullptr);
    ~DonateWindow();

public slots:
    void openDonateURL();

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};
