﻿#include "PCH.h"

#include <QApplication>
#include <QPixmap>
#include <QSplashScreen>
#include <QCoreApplication>
#include <QLibraryInfo>
#include <QLocale>
#include <QDir>

#include "Core.h"
//#include "MainWindow.h"
#include "WelcomeScreen.h"
#include "ConfigManager.h"
#include "Utils.h"

Core::Core()
{
}

int Core::exec(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qApp->processEvents();
    QPixmap splashImage(":/images/SplashScreen.png");
    QSplashScreen *splashScreen = new QSplashScreen(splashImage);
    splashScreen->show();
    qApp->processEvents();
    app.setAttribute(Qt::AA_EnableHighDpiScaling);
    app.setAttribute(Qt::AA_UseHighDpiPixmaps);    
    app.setApplicationName(QStringLiteral(APP_NAME));
    app.setApplicationDisplayName(QStringLiteral(APP_DISPLAY_NAME));
    app.setApplicationVersion(QStringLiteral(APP_VERSION));
    app.setOrganizationName(QStringLiteral(APP_PUBLISHER));
    app.setOrganizationDomain(QStringLiteral(APP_PUBLISHER_URL));
    app.setFont(QFont(QStringLiteral("Microsoft YaHei")));
    Utils::setCodec(QStringLiteral("UTF-8"));
    QString qmDir = QLibraryInfo::location(QLibraryInfo::TranslationsPath);
    QString qtQMFilePath = qmDir + QDir::separator() + QStringLiteral("qt_") + QLocale::system().name() + QStringLiteral(".qm");
    QString icQMFilePath = qmDir + QDir::separator() + QStringLiteral("ic_") + QLocale::system().name() + QStringLiteral(".qm");
    Utils::setTranslator(qtQMFilePath);
    Utils::setTranslator(icQMFilePath);
    qApp->processEvents();

    splashScreen->showMessage(QObject::tr("Creating main window...")
                             , Qt::AlignLeft | Qt::AlignBottom, Qt::white);
    qApp->processEvents();
    WelcomeScreen welcomeScreen;
    welcomeScreen.show();
    splashScreen->finish(&welcomeScreen);
    delete splashScreen;
    splashScreen = nullptr;

    return app.exec();
}
