﻿#pragma once

#include <QWidget>
#include <QStackedWidget>

#include "DraggableWidget.h"
#include "WelcomeScreen.h"

class MainWindow : public DraggableWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    WelcomeScreen *m_pWelcomePage;
    QStackedWidget *m_pContentContainer;
};
