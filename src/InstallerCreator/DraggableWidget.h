﻿/****************************************************
 * 这个类继承自QWidget                                *
 * 这个类使QWidget可以被鼠标左键按住拖动                 *
 ****************************************************/

#pragma once

#include <QWidget>

#define DEFAULT_VALID_BORDER_DISTANCE 5

class DraggableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DraggableWidget(QWidget *parent = Q_NULLPTR);
    ~DraggableWidget();

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

public slots:
    //设置窗口是否可以被鼠标左键按住拖动（鼠标右键默认不能拖动）
    void setWindowMovable(bool m_movable = true);
    //返回窗口是否可以被鼠标左键按住拖动
    bool windowMovable() const;
    //设置窗口大小是否是固定的，不允许被改变
    void setWindowSizeFixed(bool m_fixed = true);
    //返回窗口大小时候是固定不可改变的
    bool windowSizeFixed() const;
    //返回窗口是否正被鼠标左键按住
    bool windowPressed() const;
    //设置窗口边界识别鼠标的距离
    void setWindowValidBorderDistance(int m_distance = DEFAULT_VALID_BORDER_DISTANCE);
    //返回窗口边界识别鼠标的距离
    int validBorderDistance() const;

private:
    int m_uValidBorderDistance;
    bool m_bMovable;
    bool m_bPressed;
    QPoint m_pSourcePosition;
    bool m_bSizeFixed;
};
