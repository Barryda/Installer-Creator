﻿#pragma once

#include <QHBoxLayout>
#include <QScrollArea>
#include <QPushButton>

#include "DonateWindow.h"
#include "DraggableWidget.h"

class WelcomeScreen : public DraggableWidget
{
    Q_OBJECT

public:
    explicit WelcomeScreen(QWidget *parent = nullptr);
    ~WelcomeScreen();

signals:
    void projectOpened(const QString &projectFilePath);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void closeEvent(QCloseEvent *event);

public slots:
    void onMinimizeButtonClicked();
    void onMaximizeButtonClicked();
    void onCloseButtonClicked();
    void showDonateWindow();

private slots:
    void createRecentFileListPanel();
    void createTemplateChooserPanel();
    void createWindowTitleBar();
    void initForm();
    bool confirmExit();
    void saveConfig();

private:
    DonateWindow *m_pDonateWindow;
    QHBoxLayout *m_pMainLayout;
    QWidget *m_pRecentFileListPanel;
    QScrollArea *m_pTemplateChooserPanelArea;
    QScrollArea *m_pRecentFileListArea;
    QWidget *m_pWindowTitleBarPanel;
    QPushButton *m_pMinimizeButton;
    QPushButton *m_pMaximizeButton;
    QPushButton *m_pCloseButton;
};
